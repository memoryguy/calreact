var formatDate = function(d) {
  return moment(d).format('MMMM DD YYYY, HH:mm:ss');
}
