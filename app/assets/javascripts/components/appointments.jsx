var Appointments = React.createClass( {
  getInitialState: function() {
    return {
      appointments: this.props.appointments,
      title: "Team standup meeting",
      appt_time: "2017-02-11T14:34"
    }
  },

  handleUserInput: function(obj) {
    this.setState(obj);
  },

  handleFormSubmit: function() {
    var appointment = {
      title: this.state.title,
      appt_time: this.state.appt_time
    };

    $.post('/appointments', { appointment: appointment } )
      .done(function(data) {
        this.addNewAppointment(data);
      }.bind(this));
  },

  addNewAppointment: function(data) {
    var appointments = React.addons.update(this.state.appointments, { $push: [data] });

    this.setState({
      appointments: appointments.sort(function(a, b) {
        return new Date(a.appt_time) - new Date(b.appt_time);
      })
    })
  },

  render: function() {
    return (
      <div>
        <AppointmentForm title={this.state.title}
                         appt_time={this.state.appt_time}
                         onFormSubmit={this.handleFormSubmit}
                         onUserInput={this.handleUserInput} />
        <AppointmentsList appointments={this.state.appointments} />
      </div>
    );
  }
});
